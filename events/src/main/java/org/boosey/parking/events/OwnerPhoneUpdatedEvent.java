/* Generated from parking event snippet */
package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import javax.enterprise.context.ApplicationScoped;
import org.boosey.parking.events.EventBase;
import org.bson.types.ObjectId;

@ApplicationScoped
public class OwnerPhoneUpdatedEvent extends EventBase {
    public String ownerId;
    public String phone;

    public static OwnerPhoneUpdatedEvent with(ObjectId ownerId, String phone) {
        OwnerPhoneUpdatedEvent e = new OwnerPhoneUpdatedEvent();
        e.channelName = "owner-phone-updated";
        e.ownerId = getObjectIdAsString(ownerId);
        e.phone = phone;
        return e;
    }

    @Outgoing("owner-phone-updated")
    public Flowable<String> createStreamEmitter() {
        return this.createStream("owner-phone-updated");
   }
}