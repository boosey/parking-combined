/* Generated from parking event snippet */
package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import javax.enterprise.context.ApplicationScoped;
import org.boosey.parking.events.EventBase;
import org.bson.types.ObjectId;

@Slf4j
@ApplicationScoped
public class OwnerDeletedEvent extends EventBase {
    public String ownerId;

    public static OwnerDeletedEvent with(ObjectId ownerId) {
        OwnerDeletedEvent e = new OwnerDeletedEvent();
        e.channelName = "owner-deleted";
        e.ownerId = getObjectIdAsString(ownerId);
        return e;
    }

    @Outgoing("owner-deleted")
    public Flowable<String> createStreamEmitter() {
        log.info("Creating owner-deleted");
        return this.createStream("owner-deleted");
   }
}