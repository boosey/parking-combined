/* Generated from parking event snippet */
package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;
import org.boosey.parking.events.EventBase;

@ApplicationScoped
public class OwnerNameUpdatedEvent extends EventBase {
    public String ownerId;
    public String name;

    public static OwnerNameUpdatedEvent with(ObjectId ownerId, String name) {
        OwnerNameUpdatedEvent e = new OwnerNameUpdatedEvent();
        e.channelName = "owner-name-updated";
        e.ownerId = getObjectIdAsString(ownerId);
        e.name = name;
        return e;
    }

    @Outgoing("owner-name-updated")
    public Flowable<String> createStreamEmitter() {
        return this.createStream("owner-name-updated");
   }
}