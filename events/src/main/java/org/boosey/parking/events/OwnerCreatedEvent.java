/* Generated from parking event snippet */
package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Outgoing;
import javax.enterprise.context.ApplicationScoped;
import io.reactivex.Flowable;
import org.boosey.parking.events.EventBase;
import org.bson.types.ObjectId;

@ApplicationScoped
public class OwnerCreatedEvent extends EventBase {
    public String ownerId;
    public String spaceId;

    public static OwnerCreatedEvent with(ObjectId ownerId, String spaceId) {
        OwnerCreatedEvent e = new OwnerCreatedEvent();
        e.channelName = "owner-created";
        e.ownerId = getObjectIdAsString(ownerId);
        e.spaceId = spaceId;
        return e;
    }

    @Outgoing("owner-created")
    public Flowable<String> createStreamEmitter() {
        return this.createStream("owner-created");
    }    
}