package org.boosey.parking.events;

import java.util.HashMap;
import javax.enterprise.context.ApplicationScoped;
import com.google.gson.Gson;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Emitter;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;

@Slf4j
@ApplicationScoped
public class EventBase {

    public String channelName;
    public static HashMap<String, Emitter<String>> emitters = new HashMap<>();
    protected static Gson gson = new Gson();

    public void emit() {
        this.emitIf(true);
    }

    public void emitIf(Boolean doEmit) {
        if(doEmit) {
            log.info("EMITTING {} : {}", this.getChannelName(),  gson.toJson(this));
            this.getEmitter().onNext(gson.toJson(this));
        }
    }

    public static void setEmitter(String channelName, Emitter<String> emitter) {
        // log.info("Setting emitter: {} : {}", channelName, emitter);
        EventBase.emitters.put(channelName, emitter);
        // log.info("After Setting emitter: {} : {}", channelName, emitter);
    }

    public Emitter<String> getEmitter() throws RuntimeException {
        // log.info("Emitting {} : {}", this.channelName, emitters.get(this.getChannelName()));
        Emitter<String> em = emitters.get(this.getChannelName());
        if(em != null)
            return em;
        else
            throw new RuntimeException("EMITTER NOT INIALIZED");
    }

    public String getChannelName() {
        return channelName;
    }

    public static String getObjectIdAsString(ObjectId id) {
        return id.toHexString();
    }

    public Flowable<String> createStream(String channelName) {
        Flowable<String> flowable;
        // log.info("Creating flowable: {} ", channelName);

        flowable = Flowable.create(new FlowableOnSubscribe<String>() {
            @Override
            public void subscribe(FlowableEmitter<String> emitter) throws Exception {
                EventBase.setEmitter(channelName, emitter);
            }           
        }, BackpressureStrategy.BUFFER);  

        // log.info("Created flowable: {} : {}", channelName, flowable);
        return flowable;
    }   
}