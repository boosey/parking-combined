/* Generated from parking event snippet */
package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;
import org.boosey.parking.events.EventBase;

@ApplicationScoped
public class OwnerEmailUpdatedEvent extends EventBase {
    public String ownerId;
    public String email;

    public static OwnerEmailUpdatedEvent with(ObjectId ownerId, String email) {
        OwnerEmailUpdatedEvent e = new OwnerEmailUpdatedEvent();
        e.channelName = "owner-email-updated";
        e.ownerId = getObjectIdAsString(ownerId);
        e.email = email;
        return e;
    }

    @Outgoing("owner-email-updated")
    public Flowable<String> createStreamEmitter() {
        return this.createStream("owner-email-updated");
   }
}