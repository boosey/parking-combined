package org.boosey.parking.utilities;

import java.lang.reflect.Field;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import javax.enterprise.context.ApplicationScoped;

@ToString
@Data
@Slf4j
@ApplicationScoped
public class Utilities{

    public Utilities() {
        super();
    }

    public Boolean isNull(Object o){
        return o == null;
    }

    public Boolean isNotNull(Object o) {
        return !isNull(o);
    }

    public <T> T updateFromNonNullFields(@NonNull T dst, @NonNull T src) {
        log.info("In utilities");
        for (Field f : dst.getClass().getDeclaredFields()) {
            try {
                if(f.get(src) != null) {
                    f.setAccessible(true);
                    f.set(dst, f.get(src)); 
                }
            } catch (Exception e) {
                throw new RuntimeException("Error copying object fields", e);    
            }
        }
        return dst;
    } 

}