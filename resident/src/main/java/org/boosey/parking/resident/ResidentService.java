package org.boosey.parking.resident;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Optional;
import lombok.Data;
import lombok.NonNull;
// import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;

@Data
// @Slf4j
@ApplicationScoped
class ResidentService {

    public List<Resident> listResidents() {
        return Resident.listAll(); 
    }

    public long deleteAllResidents() {
        long count = Resident.count();
        Resident.<Resident>streamAll().forEach(r -> {
            r.delete();
            // events.emitId("resident-deleted", r);
        });
        return count;
    }

    // public Resident createResident(@NonNull Resident r) {
    //     try {
    //         assert(r.unit != null && r.unit.length() > 0);
    //         if(!(Resident.find("unit", r.unit).firstResultOptional().isPresent())) {
    //             r.persist(); 
    //             events.emitObject("resident-created", r);
    //             return r;
    //         } else {
    //             log.info("Resident for unit exists - creation failed: {}", r.unit);
    //             return null;
    //         }
    //     } catch (AssertionError e) {
    //         log.error("Unit not specified", e);
    //         return null;
    //     } catch (Exception e) {
    //         log.error("Unknown error creating new resident {}", r);
    //         return null;
    //     }
    // }

    public Optional<Resident> getResidentById(@NonNull String id) {
        Optional<Resident> r = Resident.findByIdOptional(new ObjectId(id));
        return r;
    }

    // public Boolean changeResidentProperties(@NonNull String id, @NonNull Resident r) {
    //     Boolean success = false;
    //     r.unit = null;
    //     try {
    //         Optional<Resident> opt_r = getResidentById(id);
    //         if (opt_r.isPresent()) {
    //             this.updateFromNonNullFields(opt_r.get(), r).update();

    //             events.emitFieldsIf(r.name != null, "resident-name-updated", r, "id", "name");
    //             events.emitFieldsIf(r.email != null, "resident-email-updated", r, "id", "email");
    //             events.emitFieldsIf(r.phone != null, "resident-phone-updated", r, "id", "phone");
    //             events.emitFieldsIf(r.residentIsResident != null, "resident-resident-is-resident-updated", r, 
    //                 "id", "residentIsResident");

    //             success = true;
    //         }
    //     } catch (Exception e) {
    //         log.error("change resident prperties: ", e);
    //     }
    //     return success;
    // }

    // private Resident updateFromNonNullFields(@NonNull Resident dst, @NonNull Resident src) {
    //     for (Field f : dst.getClass().getDeclaredFields()) {
    //         try {
    //             if(f.get(src) != null) {
    //                 f.setAccessible(true);
    //                 f.set(dst, f.get(src)); 
    //             }
    //         } catch (Exception e) {
    //             throw new RuntimeException("Error copying object fields", e);    
    //         }
    //     }
    //     return dst;
    // } 

    // @Outgoing("resident-created")
    // public Flowable<JsonObject> createResidentCreatedStream() {
    //     return events.fromChannelName("resident-created");
    // }
    // @Outgoing("resident-deleted")
    // public Flowable<JsonObject> createResidentDeletedStream() {
    //     return events.fromChannelName("resident-deleted");
    // }
    // @Outgoing("resident-name-updated")
    // public Flowable<JsonObject> createResidentNameUpdatedStream() {
    //     return events.fromChannelName("resident-name-updated");
    // }
    // @Outgoing("resident-email-updated")
    // public Flowable<JsonObject> createResidentEmailUpdatedStream() {
    //     return events.fromChannelName("resident-email-updated");
    // }
    // @Outgoing("resident-phone-updated")
    // public Flowable<JsonObject> createResidentPhoneUpdatedStream() {
    //     return events.fromChannelName("resident-phone-updated");
    // }
    // @Outgoing("resident-resident-is-resident-updated")
    // public Flowable<JsonObject> createResidentIsResidentUpdatedStream() {
    //     return events.fromChannelName("resident-resident-is-resident-updated");
    // }
}