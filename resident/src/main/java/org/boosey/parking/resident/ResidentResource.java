package org.boosey.parking.resident;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
// import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
// import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import lombok.Data;
// import lombok.NonNull;
// import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data 
@Path("/resident")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class ResidentResource {

    @Inject
    ResidentService service;

    // @PUT
    // @Path("/{id}")
    // public Response updateResident(@PathParam("id") String id, @NonNull Resident r) {
    //     try {            
    //         if (service.changeResidentProperties(id, r)) {
    //             Resident updatedResident = service.getResidentById(id).orElse(null);
    //             log.info("Resident updated {}", updatedResident);
    //             return Response.ok().entity(updatedResident).build();
    //         } else {
    //             return Response.status(404).build();
    //         }
    //     } catch (Exception e) {
    //         return Response.status(500).build();
    //     }
    // }

    @GET
    @Path("/{id}")
    public Resident getResidentById(@PathParam("id") String id) throws RuntimeException{
        try {
            Optional<Resident> r = service.getResidentById(id);
            return r.isPresent() ? r.get() : null;       

        } catch (Exception e) {
            log.info("Error getting residents from database", e);
            return null;
        }
    }

    @GET
    public List<Resident> listAll() throws RuntimeException{
        try {
            return service.listResidents();            
        } catch (Exception e) {
            log.info("Error getting residents from database", e);
            return null;
        }
    }

    // @POST
    // public Response create(@NonNull Resident resident) {
    //     try {  
    //         val r = service.createResident(resident) ;     
    //         if(r != null) {
    //             return Response.status(201).entity(o).build();
    //         } else {
    //             return Response.status(409).build();
    //         }
    //     } catch (Exception e) {
    //         return Response.status(500).build();
    //     }
    // }

    @DELETE
    public Response deleteAll() {
        try {
            service.deleteAllResidents();
            return Response.ok().build();                
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }
}