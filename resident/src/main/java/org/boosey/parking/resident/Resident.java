package org.boosey.parking.resident;

import javax.enterprise.context.ApplicationScoped;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import lombok.ToString;

@ToString
@MongoEntity(collection = "resident")
@ApplicationScoped
public class Resident extends PanacheMongoEntity {
    public String name;
    public String email;
    public String phone;
    public String ownerId;
    public String unitId;
}