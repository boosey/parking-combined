package org.boosey.parking.resident;

import io.quarkus.test.junit.QuarkusTest;
// import io.restassured.http.ContentType;
// import io.restassured.mapper.ObjectMapperType;
// import io.restassured.path.json.*;
// import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;
// import static org.hamcrest.MatcherAssert.*;
// import java.util.ArrayList;
// import com.google.gson.Gson;
// import com.google.gson.reflect.TypeToken;
// import org.hamcrest.text.*;
// import org.hamcrest.Matcher;
// import static org.hamcrest.Matchers.*;
// import org.hamcrest.beans.*;
// import static io.restassured.RestAssured.*;
// import static org.hamcrest.collection.IsEmptyCollection.empty;

@QuarkusTest
@TestInstance(Lifecycle.PER_CLASS)
public class ResidentResourceTest {
    // private static final String residentPath = "/resident";
    // private static  Resident newResident1, newResident2, newResident3;
    // protected static Gson gson = new Gson();
    // protected static TypeToken<ArrayList< Resident>>  ResidentCollectionType = new TypeToken<ArrayList< Resident>>(){};
    // private static class  Resident {
    //     public String id;
    //     public String name;
    //     public String email;
    //     public String phone;
    //     public String unitId;
    //     public String ownerId;
    // }

    // JsonPath jsonPath;

    // @BeforeAll
    // static void initTestObjects() {

    //     newResident1 = new  Resident();
    //     newResident1.name = "Boosey";
    //     newResident1.email = "bbbbb@gmail.com";
    //     newResident1.phone = "985-555-5555";
    //     newResident1.unit = "204";
    //     newResident1.residentIsResident = true;

    //     newResident2 = new  Resident();
    //     newResident2.name = "Kelly Boudreaux";
    //     newResident2.email = "kkkkkkk@gmail.com";
    //     newResident2.phone = "504-555-5555";
    //     newResident2.unit = "12345";
    //     newResident2.residentIsResident = false;

    //     newResident3 = new  Resident();
    //     newResident3.name = "Zachary Boudreaux";
    //     newResident3.email = "zzzzzzzz@gmail.com";
    //     newResident3.phone = "318-318-3188";
    //     newResident3.unit = "98543";
    //     newResident3.residentIsResident = true;
    // }

    // @Test
    // public void updateResident() {
    //     ArrayList< Resident> r_list;
    //     deleteAll();
    //     addSomeResidents();

    //     r_list = given()
    //         .when().get(residentPath)
    //         .then()
    //         .statusCode(200)
    //         .contentType(ContentType.JSON)
    //         .extract().as( ResidentCollectionType.getType());
      
    //     assertThat(o_list, hasSize(2));    

    //      Resident r = new  Resident();
    //     r.name = newResident3.name;
    //     r.email = newResident3.email;
    //     r.phone = newResident3.phone;
    //     r.unit = newResident3.unit;
    //     r.residentIsResident = newResident3.residentIsResident;

    //     given()
    //         .contentType(ContentType.JSON)
    //         .body(o)
    //     .when()
    //         .put(residentPath + "/{id}", r_list.get(0).id)
    //     .then()
    //         .statusCode(200)
    //         .contentType(ContentType.JSON);
            
    //      Resident r_chk = 
    //     getResidentById(o_list.get(0).id)
    //     .then()
    //         .statusCode(200)
    //         .contentType(ContentType.JSON)
    //         .extract().as( Resident.class, ObjectMapperType.GSON);

    //     assertThat(o_chk,  beanMatcher(newResident3, "id"));
    // }

    // @Test
    // public void unknownIdRetrieval() {
    //     getResidentById("5e377ad874760b05ddb625bf")
    //         .then()
    //             .statusCode(204);
    // }

    // public Response getResidentById(String id) {
    //     return given()
    //      .contentType(ContentType.JSON)
    //     .when()
    //         .get(residentPath + "/{id}", id);            
    // }
    
    // @Test
    // public void listAll() {

    //     ArrayList< Resident> r_list;
    //     addSomeResidents();

    //     r_list = given()
    //       .when().get(residentPath)
    //       .then()
    //          .statusCode(200)
    //          .contentType(ContentType.JSON)
    //          .extract().as( ResidentCollectionType.getType());
        
    //     assertThat(o_list, hasSize(2));
    //     assertThat(o_list.get(0),  beanMatcher(newResident1, "id"));
    //     assertThat(o_list.get(1),  beanMatcher(newResident2, "id"));
    // }

    // @Test
    // public void deleteAll() {
    //     ArrayList< Resident> r_list;

    //     given()
    //         .when().delete(residentPath)
    //         .then()
    //             .statusCode(200)
    //             .body(IsEmptyString.emptyOrNullString());

    //     r_list = given()
    //             .when().get(residentPath)
    //             .then()
    //                 .statusCode(200)
    //                 .contentType(ContentType.JSON)
    //                 .extract().as( ResidentCollectionType.getType());
        
    //     assertThat(o_list, hasSize(0));   
    //     assertThat(o_list, is(empty()));    
    // }

    // @Test
    // public void addSomeResidents() {

    //     deleteAll();

    //      Resident r = given().contentType(ContentType.JSON).body(newResident1).when().post(residentPath).then()
    //             .statusCode(201)
    //             .contentType(ContentType.JSON)
    //             .extract().as( Resident.class, ObjectMapperType.GSON);
    //             // .body(samePropertyValuesAs(newResident1, "id"));

    //     assertThat(o,  beanMatcher(newResident1, "id"));
        
    //     r = given().contentType(ContentType.JSON).body(newResident2).when().post(residentPath).then()
    //             .statusCode(201)
    //             .contentType(ContentType.JSON)
    //             .extract().as( Resident.class, ObjectMapperType.GSON);

    //     assertThat(o,  beanMatcher(newResident2, "id"));
    // }

    // protected <T> Matcher<T> beanMatcher(T r, String ...ignore) {
    //     return SamePropertyValuesAs.<T>samePropertyValuesAs(o, "id");
    // }
}

