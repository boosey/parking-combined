package org.boosey.parking.owner;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.path.json.*;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import static org.hamcrest.MatcherAssert.*;
import java.util.ArrayList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.hamcrest.text.*;
import org.hamcrest.Matcher;
import static org.hamcrest.Matchers.*;
import org.hamcrest.beans.*;
import static io.restassured.RestAssured.*;
import static org.hamcrest.collection.IsEmptyCollection.empty;

@QuarkusTest
@TestInstance(Lifecycle.PER_CLASS)
public class OwnerResourceTest {
    private static final String ownerPath = "/owner";
    private static OwnerMock newOwner1, newOwner2, newOwner3;
    protected static Gson gson = new Gson();
    protected static TypeToken<ArrayList<OwnerMock>> OwnerMockCollectionType = new TypeToken<ArrayList<OwnerMock>>(){};
    private static class OwnerMock {
        public String id;
        public String name;
        public String email;
        public String phone;
        public String spaceId;
        public Boolean ownerIsResident;
    }

    JsonPath jsonPath;

    @BeforeAll
    static void initTestObjects() {

        newOwner1 = new OwnerMock();
        newOwner1.name = "Boosey";
        newOwner1.email = "bbbbb@gmail.com";
        newOwner1.phone = "985-555-5555";
        newOwner1.spaceId = "5e41a7bf0f085c21858fdbd2";
        newOwner1.ownerIsResident = true;

        newOwner2 = new OwnerMock();
        newOwner2.name = "Kelly Boudreaux";
        newOwner2.email = "kkkkkkk@gmail.com";
        newOwner2.phone = "504-555-5555";
        newOwner2.spaceId = "5e41a7bf0f085c21858fdbd3";
        newOwner2.ownerIsResident = false;

        newOwner3 = new OwnerMock();
        newOwner3.name = "Zachary Boudreaux";
        newOwner3.email = "zzzzzzzz@gmail.com";
        newOwner3.phone = "318-318-3188";
        newOwner3.spaceId = "5e41a7bf0f085c21858fdbd4";
        newOwner3.ownerIsResident = false;
    }

    @Test
    public void updateOwner() {
        ArrayList<OwnerMock> o_list;
        deleteAll();
        addSomeOwners();

        o_list = given()
            .when().get(ownerPath)
            .then()
            .statusCode(200)
            .contentType(ContentType.JSON)
            .extract().as(OwnerMockCollectionType.getType());
      
        assertThat(o_list, hasSize(2));    

        OwnerMock o = new OwnerMock();
        o.name = newOwner3.name;
        o.email = newOwner3.email;
        o.phone = newOwner3.phone;
        o.spaceId = newOwner3.spaceId;
        o.ownerIsResident = newOwner3.ownerIsResident;

        given()
            .contentType(ContentType.JSON)
            .body(o)
        .when()
            .put(ownerPath + "/{id}", o_list.get(0).id)
        .then()
            .statusCode(200)
            .contentType(ContentType.JSON);
            
        OwnerMock o_chk = 
        getOwnerById(o_list.get(0).id)
        .then()
            .statusCode(200)
            .contentType(ContentType.JSON)
            .extract().as(OwnerMock.class, ObjectMapperType.GSON);

        assertThat(o_chk,  beanMatcher(newOwner3, "id"));
    }

    // @Test
    public void unknownIdRetrieval() {
        getOwnerById("5e377ad874760b05ddb625bf")
            .then()
                .statusCode(204);
    }

    public Response getOwnerById(String id) {
        return given()
         .contentType(ContentType.JSON)
        .when()
            .get(ownerPath + "/{id}", id);            
    }
    
    @Test
    public void listAll() {

        ArrayList<OwnerMock> o_list;
        addSomeOwners();

        o_list = given()
          .when().get(ownerPath)
          .then()
             .statusCode(200)
             .contentType(ContentType.JSON)
             .extract().as(OwnerMockCollectionType.getType());
        
        assertThat(o_list, hasSize(2));
        assertThat(o_list.get(0),  beanMatcher(newOwner1, "id"));
        assertThat(o_list.get(1),  beanMatcher(newOwner2, "id"));
    }

    @Test
    public void deleteAll() {
        ArrayList<OwnerMock> o_list;

        given()
            .when().delete(ownerPath)
            .then()
                .statusCode(200)
                .body(IsEmptyString.emptyOrNullString());

        o_list = given()
                .when().get(ownerPath)
                .then()
                    .statusCode(200)
                    .contentType(ContentType.JSON)
                    .extract().as(OwnerMockCollectionType.getType());
        
        assertThat(o_list, hasSize(0));   
        assertThat(o_list, is(empty()));    
    }

    @Test
    public void addSomeOwners() {

        deleteAll();

        OwnerMock o = given().contentType(ContentType.JSON).body(newOwner1).when().post(ownerPath).then()
                .statusCode(201)
                .contentType(ContentType.JSON)
                .extract().as(OwnerMock.class, ObjectMapperType.GSON);
                // .body(samePropertyValuesAs(newOwner1, "id"));

        assertThat(o,  beanMatcher(newOwner1, "id"));
        
        o = given().contentType(ContentType.JSON).body(newOwner2).when().post(ownerPath).then()
                .statusCode(201)
                .contentType(ContentType.JSON)
                .extract().as(OwnerMock.class, ObjectMapperType.GSON);

        assertThat(o,  beanMatcher(newOwner2, "id"));
    }

    protected <T> Matcher<T> beanMatcher(T o, String ...ignore) {
        return SamePropertyValuesAs.<T>samePropertyValuesAs(o, "id");
    }
}

