package org.boosey.parking.owner;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import lombok.Data;
import lombok.NonNull;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data 
@Path("/owner")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class OwnerResource {

    @Inject
    OwnerService service;

    @PUT
    @Path("/{id}")
    public Response updateOwner(@PathParam("id") String id, @NonNull Owner o) {
        try {            
            if (service.changeOwnerProperties(id, o)) {
                Owner updatedOwner = service.getOwnerById(id).orElse(null);
                log.info("Owner updated {}", updatedOwner);
                return Response.ok().entity(updatedOwner).build();
            } else {
                return Response.status(404).build();
            }
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }

    @GET
    @Path("/{id}")
    public Owner getOwnerById(@PathParam("id") String id) throws RuntimeException{
        try {
            Optional<Owner> o = service.getOwnerById(id);
            return o.isPresent() ? o.get() : null;       

        } catch (Exception e) {
            log.info("Error getting owners from database", e);
            return null;
        }
    }

    @GET
    public List<Owner> listAll() throws RuntimeException{
        try {
            return service.listOwners();            
        } catch (Exception e) {
            log.info("Error getting owners from database", e);
            return null;
        }
    }

    @POST
    public Response create(@NonNull Owner owner) {
        try {  
            val o = service.createOwner(owner) ;     
            if(o != null) {
                return Response.status(201).entity(o).build();
            } else {
                return Response.status(409).build();
            }
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }

    @DELETE
    public Response deleteAll() {
        try {
            service.deleteAllOwners();
            return Response.ok().build();                
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }
}