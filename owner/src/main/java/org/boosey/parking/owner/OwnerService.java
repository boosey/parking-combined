package org.boosey.parking.owner;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import org.boosey.parking.events.OwnerCreatedEvent;
import org.boosey.parking.events.OwnerDeletedEvent;
import org.boosey.parking.events.OwnerIsResidentUpdatedEvent;
import org.boosey.parking.events.OwnerEmailUpdatedEvent;
import org.boosey.parking.events.OwnerNameUpdatedEvent;
import org.boosey.parking.events.OwnerPhoneUpdatedEvent;
import org.boosey.parking.utilities.Utilities;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;

@Data
@Slf4j
@ApplicationScoped
class OwnerService {
    @Inject public Utilities u;

    public List<Owner> listOwners() {
        return Owner.listAll(); 
    }

    public long deleteAllOwners() {
        long count = Owner.count();
        Owner.<Owner>streamAll().forEach(o -> {
            o.delete();
            OwnerDeletedEvent.with(o.id).emit();
        });
        return count;
    }

    public Owner createOwner(@NonNull Owner o) {
        try {
            assert(o.spaceId != null && o.spaceId.length() > 0);
            if(!(Owner.find("spaceId", o.spaceId).firstResultOptional().isPresent())) {
                o.persist(); 
                OwnerCreatedEvent.with(o.id, o.spaceId).emit();
                return o;
            } else {
                log.info("Owner for unit exists - creation failed: {}", o.spaceId);
                return null;
            }
        } catch (AssertionError e) {
            log.error("Unit not specified", e);
            return null;
        } catch (Exception e) {
            log.error("Unknown error creating new owner {}", o);
            return null;
        }
    }

    public Optional<Owner> getOwnerById(@NonNull String id) {
        Optional<Owner> o = Owner.findByIdOptional(new ObjectId(id));
        return o;
    }

    public Boolean changeOwnerProperties(@NonNull String id, @NonNull Owner o) {
        Boolean success = false;
        o.spaceId = null;
        try {
            Optional<Owner> opt_o = getOwnerById(id);
            if (opt_o.isPresent()) {
                u.updateFromNonNullFields(opt_o.get(), o).update();

                OwnerNameUpdatedEvent.with(new ObjectId(id), o.name).emitIf(o.name != null);
                OwnerPhoneUpdatedEvent.with(new ObjectId(id), o.phone).emitIf(o.phone != null);
                OwnerEmailUpdatedEvent.with(new ObjectId(id), o.email).emitIf(o.email != null);
                OwnerIsResidentUpdatedEvent.with(new ObjectId(id), o.ownerIsResident).emitIf(o.ownerIsResident != null);
                success = true;
            }
        } catch (Exception e) {
            log.error("change owner prperties: ", e);
        }
        return success;
    }
}