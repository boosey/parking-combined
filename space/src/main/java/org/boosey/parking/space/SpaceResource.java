package org.boosey.parking.space;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.Data;
import lombok.NonNull;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data 
@Path("/space")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class SpaceResource {

    @Inject 
    SpaceService service;

    @POST
    public Response create(@NonNull Space space) {
        try {  
            val o = service.createSpace(space) ;     
            if(o != null) {
                return Response.status(201).entity(o).build();
            } else {
                return Response.status(409).build();
            }
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }

    @GET
    public List<Space> listAll(){
        try {
            return service.listSpaces();            
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    @GET
    @Path("/{id}")
    public Space getSpaceById(@PathParam("id") String id) throws RuntimeException {
        try {
            Optional<Space> o = service.getSpaceById(id);
            return o.isPresent() ? o.get() : null;       

        } catch (Exception e) {
            log.info("Error getting space from database", e);
            return null;
        }
    }

    @DELETE
    public Response deleteAll() {
        try {
            log.info("IN @DELETE RESOURCE METHOD");
            log.info("SPACESERVICE == {}", service.toString());
            service.deleteAllSpaces();
            return Response.ok().build();                
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }
}