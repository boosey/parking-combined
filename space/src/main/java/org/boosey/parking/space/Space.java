package org.boosey.parking.space;

import javax.enterprise.context.ApplicationScoped;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import lombok.ToString;

@ToString
@MongoEntity(collection = "space")
@ApplicationScoped
public class Space extends PanacheMongoEntity {
    public String number;
    public String ownerId;
    public String residentId;
    public Boolean residentApprovedByOwner;

    public Space() {
        super();
    }

    private Space(Space s) {
        // super();
        this.id = s.id;
        this.number = s.number;
        this.ownerId = s.ownerId;
        this.residentApprovedByOwner = s.residentApprovedByOwner;
        this.residentId = s.residentId;
    }

    public static Space from(Space s) {
        return new Space(s);
    }
}