package org.boosey.parking.space;

// import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import com.google.gson.Gson;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.boosey.parking.events.*;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;

@Data
@Slf4j
@ApplicationScoped
class SpaceService {

    private static Gson gson = new Gson();
    
    public List<Space> listSpaces() {
        return Space.listAll();
    } 

    public Optional<Space> getSpaceById(@NonNull String id) {
        Optional<Space> o = Space.findByIdOptional(new ObjectId(id));
        return o;
    }

    public long deleteAllSpaces() {
        log.info("In DELETE ALL SPACES");
        long count = Space.count();
        log.info("FOUND {} SPACES", count);
        Space.<Space>streamAll().forEach(o -> {
            o.delete();
            // SpaceDeletedEvent.with(o.id).emit();
        });
        log.info("LEAVING DELETE ALL SPACES");
        return count;
    }

    public Space createSpace(Space s) {
        Space spc = new Space();
        spc.number = s.number;
        spc.residentApprovedByOwner = false;
        spc.persist();
        // SpaceOwnerChangedEvent.with(spc.id, spc.number).emit();;
        log.info("Created space {}", spc);

        return spc;
    }

    @Incoming("owner-created-received")
    public void handleOwnerCreated(@NonNull String oc_evt_in) {

        OwnerCreatedEvent oc_evt = gson.fromJson(oc_evt_in, OwnerCreatedEvent.class);
        log.info("oc_evt: {}", oc_evt);
        
        try {
            log.info("OWNER CREATED RECEIVED owner: {} space: {}", oc_evt.ownerId, oc_evt.spaceId);
            Optional<Space> o_spc = Space.findByIdOptional(new ObjectId(oc_evt.spaceId));
            if((o_spc.isPresent())) {
                // Change owner on existing space
                // Space spc_orig = Space.from(o_spc.get());
                Space spc = o_spc.get();

                spc.ownerId = oc_evt.ownerId;
                // When new Owner is created the Space's resident must be set and approved again
                spc.residentId = null;
                spc.residentApprovedByOwner = false;

                spc.update();

                // Always emit
                // SpaceOwnerChangedEvent.with(spc.id, spc.ownerId).emit();

                // // Only emit if resident was orginally set
                // event.emitIf(spc_orig.residentId != null, "space-resident-removed", spc_orig, residentRemovedEventFields);
                // // Only emit if approved previously; With new owner the resident must be re-associated and re-approved
                // event.emitIf(spc_orig.residentApprovedByOwner != null && spc_orig.residentApprovedByOwner == true, 
                //     "space-resident-approval-by-owner-removed", spc_orig, residentApprovalRemovedEventFields);
                log.info("Updated space owner {}", spc);

            } else {
                throw new Exception("No such space");
            }
        } catch (Exception e) {
            log.error("Error processing owner-created", e);   
        }
    }

    // @Incoming("owner-deleted-received")
    // public void handleOwnerDeleted(@NonNull JsonObject o_evt_in) {
    //     JsonObject o_evt = o_evt_in.getJsonObject("map");

    //     try {
    //         Optional<Space> o_spc = Space.find("owner", o_evt.getString("id")).firstResultOptional();
    //         if((o_spc.isPresent())) {
    //             // Remove owner on existing space
    //             Space spc = o_spc.get();
    //             Space spc_orig = Space.from(o_spc.get());

    //             spc.ownerId = null;
    //             spc.residentId = null;
    //             spc.residentApprovedByOwner = false;
    //             spc.update();

    //             event.emit("space-owner-removed", spc_orig, ownerDeletedEventFields);
    //              // Only emit if resident was orginally set
    //              event.emitIf(spc_orig.residentId != null, "space-resident-removed", spc_orig, residentRemovedEventFields);
    //              // Only emit if approved previously; With new owner the resident must be re-associated and re-approved
    //              event.emitIf(spc_orig.residentApprovedByOwner != null && spc_orig.residentApprovedByOwner == true, 
    //                  "space-resident-approval-by-owner-removed", spc_orig, residentApprovalRemovedEventFields);
    //              log.info("Updated space owner {}", spc);

    //         } else {
    //             log.info("No space for deleted owner {}", o_evt);
    //         }
    //     } catch (Exception e) {
    //         log.error("Error processing owner-deleted", e);   
    //     }
    // }

    // @Incoming("resident-created-received")
    // public void handleResidentCreated(@NonNull JsonObject oc_evt_in) {
    //     JsonObject oc_evt = oc_evt_in.getJsonObject("map");

    //     try {
    //         // Event contains the residentId and ownerId only
    //         log.info("oc_evt: ", oc_evt);
            
    //         Optional<Space> o_spc = Space.find("id", oc_evt.getString("spaceId")).firstResultOptional();
    //         if((o_spc.isPresent())) {
    //             // Change owner on existing space
    //             Space spc = o_spc.get();
    //             Space spc_orig = Space.from(o_spc.get());

    //             spc.residentId = oc_evt.getString("id");
    //             spc.residentApprovedByOwner = false;
    //             spc.update();

    //             event.emitIf(spc_orig.residentId != null, "space-resident-changed", spc_orig, residentChangedEventFields);
    //             event.emitIf(spc_orig.residentApprovedByOwner != null && spc_orig.residentApprovedByOwner == true, 
    //                 "space-resident-approval-by-owner-removed", spc_orig, residentApprovalRemovedEventFields);

    //              log.info("Updated space residenrt {}", spc);

    //         } else {
    //             throw new Exception("Space doesn't exist");
    //         }
    //     } catch (Exception e) {
    //         log.error("Error processing owner-created", e);            
    //     }
    // }

    // @Incoming("resident-deleted-received")
    // public void handleResidentDeleted(@NonNull JsonObject o_evt_in) {
    //     JsonObject o_evt = o_evt_in.getJsonObject("map");

    //     try {
    //         Optional<Space> o_spc = Space.find("owner", o_evt.getString("id")).firstResultOptional();
    //         if((o_spc.isPresent())) {
    //             // Remove owner on existing space
    //             Space spc_orig = Space.from(o_spc.get());
    //             Space spc = o_spc.get();
    //             spc.residentId = null;
    //             spc.residentApprovedByOwner = false;
    //             spc.update();

    //             event.emit("space-resident-removed", spc_orig, residentRemovedEventFields);
    //             event.emit("space-resident-approval-by-owner-removed", spc_orig, residentApprovalRemovedEventFields);
    //             log.info("Updated space owner {}", spc);

    //         } else {
    //             throw new Exception("Space doesn't exist");
    //         }
    //     } catch (Exception e) {
    //         log.error("Error processing owner-deleted", e);
            
    //     }
    // }

    // @Incoming("owner-approved-resident")
    // public void handleResidentApprovelReceived(@NonNull JsonObject o_evt_in) {
    //     JsonObject o_evt = o_evt_in.getJsonObject("map");
    //     try {
    //         Optional<Space> o_spc = Space.find("owner", o_evt.getString("id")).firstResultOptional();
    //         if((o_spc.isPresent())) {
    //             Space spc = o_spc.get();

    //             if(spc.residentId != null) {
    //                 spc.residentApprovedByOwner = true;
    //                 spc.update();
    //                 event.emitId("space-resident-approved-by-owner", spc);
    //             } else {
    //                 throw new Exception("Resident not set");
    //             }
    //        } else {
    //             throw new Exception("Space doesn't exist");
    //         }
    //     } catch (Exception e) {
    //         log.error("Error processing owner-deleted", e);
            
    //     }
    //  }

    //  @Incoming("owner-disapproved-resident")
    //  public void handleResidentDisapprovalReceived(@NonNull JsonObject o_evt_in) {
    //      JsonObject o_evt = o_evt_in.getJsonObject("map");
    //      try {
    //          Optional<Space> o_spc = Space.find("owner", o_evt.getString("id")).firstResultOptional();
    //          if((o_spc.isPresent())) {
    //              Space spc = o_spc.get();
 
    //              if(spc.residentId != null) {
    //                  spc.residentApprovedByOwner = false;
    //                  spc.update();
    //                  event.emitId("space-resident-approved-by-owner", spc);
    //              } else {
    //                  throw new Exception("Resident not set");
    //              }
    //         } else {
    //              throw new Exception("Space doesn't exist");
    //          }
    //      } catch (Exception e) {
    //          log.error("Error processing owner-deleted", e);
             
    //      }
    //   }

    // @Outgoing("space-created")
    // public Flowable<JsonObject> createSpaceCreatedStream() {
    //     return event.fromChannelName("space-created");
    // }

    // @Outgoing("space-deleted")
    // public Flowable<JsonObject> createSpaceDeletedStream() {
    //     return event.fromChannelName("space-created");
    // }

    // @Outgoing("space-owner-changed")
    // public Flowable<JsonObject> createOwnerChangedCreatedStream() {
    //     return event.fromChannelName("space-owner-changed");
    // }

    // @Outgoing("space-owner-removed")
    // public Flowable<JsonObject> createOwnerRemovedStream() {
    //     return event.fromChannelName("space-owner-removed");
    // }    
    
    // @Outgoing("space-resident-changed")
    // public Flowable<JsonObject> createResidentChangedCreatedStream() {
    //     return event.fromChannelName("space-owner-changed");
    // }

    // @Outgoing("space-resident-removed")
    // public Flowable<JsonObject> createResidentRemovedStream() {
    //     return event.fromChannelName("space-owner-removed");
    // }

    // @Outgoing("space-resident-approved-by-owner")
    // public Flowable<JsonObject> createResidentAprrovedStream() {
    //     return event.fromChannelName("space-resident-approved-by-owner");
    // }

    // @Outgoing("space-resident-approval-by-owner-removed")
    // public Flowable<JsonObject> createResidentAprrovalRemovedStream() {
    //     return event.fromChannelName("space-resident-approval-by-owner-removed");
    // }
}
